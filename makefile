all: source sourcesink mapped_sink

source: source.cpp
	g++     source.cpp   -o source -lboost_iostreams

sourcesink: sourcesink.cpp
	g++     sourcesink.cpp   -o sourcesink -lboost_iostreams

mapped_sink: mapped_sink.cpp
	g++     mapped_sink.cpp   -o mapped_sink -lboost_iostreams -lboost_thread

