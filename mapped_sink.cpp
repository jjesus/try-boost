#include <iostream>
#include <fstream>
#include <boost/iostreams/device/mapped_file.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>


void server_thread(const std::string &mem_file_name)
{
  std::cerr << "server_thread running ..."  << std::endl;
  std::ofstream out("test_iostream_shmem.txt", std::ios::app);
  bool done = false;

  while (!done)
  {
    try
    {
      boost::iostreams::stream <boost::iostreams::mapped_file_source> in(mem_file_name);

      done = true;

      std::string line;
      while (std::getline(in, line, '\f'))
        if (!in.eof() && !in.fail() && !in.bad())
          out << line;
    }
    catch (const std::exception &e)
    {
      std::cerr << e.what() << std::endl;
      boost::xtime xt;
      boost::xtime_get(&xt, boost::TIME_UTC);
      xt.sec += 1;
      boost::thread::sleep(xt); // Sleep
    }
  }
  std::cerr << "server_thread returning ..."  << std::endl;
}


void doWork()
{
    std::cerr << "doWork() running ..."  << std::endl;
    std::string memory_file_name("boost.logging.shared.memory.segment");
    std::cerr << "thread declared ..."  << std::endl;
    boost::thread thrd(boost::bind(&server_thread, memory_file_name));
    std::cerr << "return from thread start ..."  << std::endl;

    boost::iostreams::stream <boost::iostreams::mapped_file_sink>
      *out  =
      new boost::iostreams::stream <boost::iostreams::mapped_file_sink>();

    std::cerr << "opening mapped_file ..."  << std::endl;
    boost::iostreams::mapped_file_params p(memory_file_name);
    p.new_file_size = 1024 * sizeof (char);
    out->open(boost::iostreams::mapped_file_sink(p));

    out->write("Hello\f", 6);

    out->close();
    std::cerr << "thread join ..."  << std::endl;
    thrd.join();
    std::cerr << "thread join returned ..."  << std::endl;
}

int main()
{
    doWork();
}


